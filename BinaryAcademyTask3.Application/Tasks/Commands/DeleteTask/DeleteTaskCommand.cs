﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace BinaryAcademyTask3.Application.Tasks.Commands.DeleteTask
{
    [DataContract]
    public class DeleteTaskCommand : IRequest
    {
        [DataMember]
        public int Id { get; set; }
    }
}
