﻿using BinaryAcademyTask3.Domain;
using BinaryAcademyTask3.Infrastructure;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BinaryAcademyTask3.Application.Tasks.Commands.DeleteTask
{
    public class DeleteTaskCommandHandler : IRequestHandler<DeleteTaskCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IGenericRepository<Domain.Task> _repository;

        public DeleteTaskCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _repository = unitOfWork.GetRepository<Domain.Task>();
        }

        public Task<Unit> Handle(DeleteTaskCommand request, CancellationToken cancellationToken)
        {
            if (request is null)
                throw new ArgumentNullException(nameof(request));

            var taskForDelete = _repository.GetById(request.Id);

            if (taskForDelete is null)
                throw new ArgumentNullException(nameof(taskForDelete));

            _repository.Remove(taskForDelete);
            _unitOfWork.Commit();

            return System.Threading.Tasks.Task.FromResult(Unit.Value);
        }
    }
}
