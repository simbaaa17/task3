﻿using AutoMapper;
using BinaryAcademyTask3.Application.Tasks.Queries;
using BinaryAcademyTask3.Infrastructure;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BinaryAcademyTask3.Application.Tasks.Commands.UpdateTask
{
    public class UpdateTaskCommandHandler : IRequestHandler<UpdateTaskCommand, TaskDto>
    {

        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UpdateTaskCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }
        public Task<TaskDto> Handle(UpdateTaskCommand request, CancellationToken cancellationToken)
        {
            if (request is null)
                throw new ArgumentNullException(nameof(request));

            var task = _unitOfWork.GetRepository<Domain.Task>().GetById(request.Id);

            if (task is null)
                throw new ArgumentNullException(nameof(task));

            task.Name = request.Name;
            task.Description = request.Description;
            task.TaskState = request.TaskState;
            task.PerformerId = request.PerformerId;
            task.ProjectId = request.PerformerId;

            _unitOfWork.GetRepository<Domain.Task>().Update(task);
            _unitOfWork.Commit();

            return System.Threading.Tasks.Task.FromResult(_mapper.Map<TaskDto>(task));
        }
    }
}
