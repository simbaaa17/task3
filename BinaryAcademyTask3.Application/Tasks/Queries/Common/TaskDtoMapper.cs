﻿using AutoMapper;
using BinaryAcademyTask3.Application.Tasks.Commands.AddTask;
using BinaryAcademyTask3.Application.Tasks.Queries.Common;
using BinaryAcademyTask3.Common;
using BinaryAcademyTask3.Domain;
using BinaryAcademyTask3.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace BinaryAcademyTask3.Application.Tasks.Queries
{
    public class TaskDtoMapper : Profile
    {
        public TaskDtoMapper()
        {
            CreateMap<Task, TaskDto>();
            CreateMap<AddTaskCommand, Task>();
            CreateMap<TaskShortInformationRecord, TaskShortInformationDto>();
            CreateMap<TasksCountInUserRecord, TasksCountInUserDto>();
        }
    }
}
