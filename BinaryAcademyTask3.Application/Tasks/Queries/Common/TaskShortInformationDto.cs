﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BinaryAcademyTask3.Common
{
    public class TaskShortInformationDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
