﻿using BinaryAcademyTask3.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace BinaryAcademyTask3.Application.Tasks.Queries.Common
{
    public class TasksCountInUserDto
    {
        public Dictionary<Project, int> Tasks { get; set; }
    }
}
