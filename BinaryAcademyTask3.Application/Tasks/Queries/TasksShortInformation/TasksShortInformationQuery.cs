﻿using BinaryAcademyTask3.Common;
using MediatR;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace BinaryAcademyTask3.Application.Tasks.Queries.TasksShortInformation
{
    [DataContract]
    public class TasksShortInformationQuery : IRequest<IEnumerable<TaskShortInformationDto>>
    {
        public int Id { get; set; }
    }
}
