﻿using AutoMapper;
using BinaryAcademyTask3.Application.Tasks.Queries.Common;
using BinaryAcademyTask3.Infrastructure;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BinaryAcademyTask3.Application.Tasks.Queries.TasksInProjectsByUser
{
    public class TasksInProjectByUserQueryHandler : IRequestHandler<TasksInProjectByUserQuery, TasksCountInUserDto>
    {
        private readonly TaskRepository _repository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public TasksInProjectByUserQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _repository = (TaskRepository)unitOfWork.GetRepository<Domain.Task>(true);
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }
        public Task<TasksCountInUserDto> Handle(TasksInProjectByUserQuery request, CancellationToken cancellationToken)
        {
            if (request is null)
                throw new ArgumentNullException(nameof(request));

            var tasksForUserRecord = _repository.GetTasksCountForUser(request.Id);

            return Task.FromResult(_mapper.Map<TasksCountInUserDto>(tasksForUserRecord));
        }
    }
}
