﻿using BinaryAcademyTask3.Application.Tasks.Queries.Common;
using MediatR;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace BinaryAcademyTask3.Application.Tasks.Queries.TasksInProjectsByUser
{
    [DataContract]
    public class TasksInProjectByUserQuery : IRequest<TasksCountInUserDto>
    {
        [DataMember]
        public int Id { get; set; }
    }
}
