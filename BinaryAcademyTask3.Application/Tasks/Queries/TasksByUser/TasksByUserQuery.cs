﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace BinaryAcademyTask3.Application.Tasks.Queries.TasksByUser
{
    [DataContract]
    public class TasksByUserQuery : IRequest<IEnumerable<TaskDto>>
    {
        public int Id { get; set; }
    }
}
