﻿using AutoMapper;
using BinaryAcademyTask3.Infrastructure;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BinaryAcademyTask3.Application.Tasks.Queries.TasksByUser
{

    public class TasksByUserQueryHandler : IRequestHandler<TasksByUserQuery, IEnumerable<TaskDto>>
    {
        private readonly TaskRepository _repository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public TasksByUserQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _repository = (TaskRepository)unitOfWork.GetRepository<Domain.Task>(true);
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public Task<IEnumerable<TaskDto>> Handle(TasksByUserQuery request, CancellationToken cancellationToken)
        {
            if (request is null)
                throw new ArgumentNullException(nameof(request));

            var tasks = _repository.GetTasksForUser(request.Id);

            return System.Threading.Tasks.Task.FromResult(_mapper.Map<IEnumerable<TaskDto>>(tasks));
        }
    }
}
