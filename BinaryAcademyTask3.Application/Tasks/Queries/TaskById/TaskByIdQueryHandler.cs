﻿using AutoMapper;
using BinaryAcademyTask3.Domain;
using BinaryAcademyTask3.Infrastructure;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BinaryAcademyTask3.Application.Tasks.Queries.TaskById
{
    public class TaskByIdQueryHandler : IRequestHandler<TaskByIdQuery, TaskDto>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public TaskByIdQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }
        public Task<TaskDto> Handle(TaskByIdQuery request, CancellationToken cancellationToken)
        {
            if (request is null)
                throw new ArgumentNullException(nameof(request));

            var task = _unitOfWork.GetRepository<Domain.Task>().GetById(request.Id);

            if (task is null)
                throw new NotImplementedException(nameof(request.Id));

            return System.Threading.Tasks.Task.FromResult(_mapper.Map<TaskDto>(task));
        }
    }
}
