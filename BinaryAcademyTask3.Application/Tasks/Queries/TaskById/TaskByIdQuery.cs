﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace BinaryAcademyTask3.Application.Tasks.Queries.TaskById
{
    [DataContract]
    public class TaskByIdQuery : IRequest<TaskDto>
    {
        public int Id { get; set; }
    }
}
