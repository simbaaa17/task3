﻿using AutoMapper;
using BinaryAcademyTask3.Infrastructure;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BinaryAcademyTask3.Application.Tasks.Queries
{
    public class TasksQueryHandler : IRequestHandler<TasksQuery, IEnumerable<TaskDto>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public TasksQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }
        public Task<IEnumerable<TaskDto>> Handle(TasksQuery request, CancellationToken cancellationToken)
        {
            if (request is null)
                throw new ArgumentNullException(nameof(request));

            var tasks = _mapper.Map<IEnumerable<TaskDto>>(_unitOfWork.GetRepository<Domain.Task>().GetAll());

            return Task.FromResult(tasks);
        }
    }
}
