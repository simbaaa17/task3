﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace BinaryAcademyTask3.Application.Tasks.Queries
{
    public class TasksQuery : IRequest<IEnumerable<TaskDto>>
    {
    }
}
