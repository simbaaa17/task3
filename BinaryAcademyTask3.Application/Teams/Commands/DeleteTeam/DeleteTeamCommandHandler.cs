﻿using BinaryAcademyTask3.Domain;
using BinaryAcademyTask3.Infrastructure;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BinaryAcademyTask3.Application.Teams.Commands.DeleteTeam
{
    public class DeleteTeamCommandHandler : IRequestHandler<DeleteTeamCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IGenericRepository<Team> _repository;

        public DeleteTeamCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _repository = unitOfWork.GetRepository<Team>();
        }

        public Task<Unit> Handle(DeleteTeamCommand request, CancellationToken cancellationToken)
        {
            if (request is null)
                throw new ArgumentNullException(nameof(request));

            var teamForDelete = _repository.GetById(request.Id);

            if (teamForDelete is null)
                throw new ArgumentNullException(nameof(teamForDelete));

            _repository.Remove(teamForDelete);
            _unitOfWork.Commit();

            return System.Threading.Tasks.Task.FromResult(Unit.Value);
        }
    }
}
