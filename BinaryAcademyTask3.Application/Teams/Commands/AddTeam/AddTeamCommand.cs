﻿using BinaryAcademyTask3.Application.Teams.Queries;
using MediatR;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace BinaryAcademyTask3.Application.Teams.Commands.AddTeam
{
    [DataContract]
    public class AddTeamCommand : IRequest<int>
    {
        [DataMember]
        public int Id { get; set; }
        
        [DataMember]
        public string Name { get; set; }
        
        [DataMember]
        public DateTime CreatedAt { get; set; }

    }
}
