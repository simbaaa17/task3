﻿using AutoMapper;
using BinaryAcademyTask3.Application.Teams.Queries;
using BinaryAcademyTask3.Domain;
using BinaryAcademyTask3.Infrastructure;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BinaryAcademyTask3.Application.Teams.Commands.UpdateTeam
{
    public class UpdateTeamCommandHandler : IRequestHandler<UpdateTeamCommand, TeamDto>
    {

        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UpdateTeamCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }
        public Task<TeamDto> Handle(UpdateTeamCommand request, CancellationToken cancellationToken)
        {
            if (request is null)
                throw new ArgumentNullException(nameof(request));

            var team = _unitOfWork.GetRepository<Team>().GetById(request.Id);

            if (team is null)
                throw new ArgumentNullException(nameof(team));

            team.Name = request.Name;

            _unitOfWork.GetRepository<Team>().Update(team);
            _unitOfWork.Commit();

            return System.Threading.Tasks.Task.FromResult(_mapper.Map<TeamDto>(team));
        }
    }
}
