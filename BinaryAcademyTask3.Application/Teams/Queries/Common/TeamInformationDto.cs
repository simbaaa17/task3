﻿using BinaryAcademyTask3.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace BinaryAcademyTask3.Application.Teams.Queries.Common
{
    public class TeamInformationDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public IEnumerable<User> Members { get; set; }
    }
}
