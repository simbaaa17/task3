﻿using AutoMapper;
using BinaryAcademyTask3.Application.Teams.Commands.AddTeam;
using BinaryAcademyTask3.Application.Teams.Queries.Common;
using BinaryAcademyTask3.Domain;
using BinaryAcademyTask3.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace BinaryAcademyTask3.Application.Teams.Queries
{
    public class TeamDtoMapper : Profile
    {
        public TeamDtoMapper()
        {
            CreateMap<Team, TeamDto>();
            CreateMap<AddTeamCommand, Team>();
            CreateMap<TeamInformationRecord, TeamInformationDto>();
        }
    }
}
