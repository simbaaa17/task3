﻿using AutoMapper;
using BinaryAcademyTask3.Domain;
using BinaryAcademyTask3.Infrastructure;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BinaryAcademyTask3.Application.Teams.Queries
{
    public class TeamsQueryHandler : IRequestHandler<TeamsQuery, IEnumerable<TeamDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public TeamsQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public Task<IEnumerable<TeamDto>> Handle(TeamsQuery request, CancellationToken cancellationToken)
        {
            if (request is null)
                throw new ArgumentNullException(nameof(request));

            var teams = _mapper.Map<IEnumerable<TeamDto>>(_unitOfWork.GetRepository<Team>().GetAll());

            return System.Threading.Tasks.Task.FromResult(teams);
        }
    }
}
