﻿using AutoMapper;
using BinaryAcademyTask3.Application.Teams.Queries.Common;
using BinaryAcademyTask3.Domain;
using BinaryAcademyTask3.Infrastructure;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BinaryAcademyTask3.Application.Teams.Queries.TeamsInformationFull
{
    public class TeamsInformationFullQueryHandler : IRequestHandler<TeamsInformationFullQuery, IEnumerable<TeamInformationDto>>
    {
        private readonly TeamRepository _repository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public TeamsInformationFullQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _repository = (TeamRepository)unitOfWork.GetRepository<Team>(true);
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }


        public Task<IEnumerable<TeamInformationDto>> Handle(TeamsInformationFullQuery request, CancellationToken cancellationToken)
        { 
            if (request is null)
                throw new ArgumentNullException(nameof(request));

            var teamsInformationRecord = _repository.GetMembersForTeams();

            return System.Threading.Tasks.Task.FromResult(_mapper.Map<IEnumerable<TeamInformationDto>>(teamsInformationRecord));
        }
    }
}
