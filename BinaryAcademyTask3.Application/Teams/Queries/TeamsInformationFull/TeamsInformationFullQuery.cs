﻿using BinaryAcademyTask3.Application.Teams.Queries.Common;
using MediatR;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace BinaryAcademyTask3.Application.Teams.Queries.TeamsInformationFull
{
    [DataContract]
    public class TeamsInformationFullQuery : IRequest<IEnumerable<TeamInformationDto>>
    {
    }
}
