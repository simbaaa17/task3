﻿using AutoMapper;
using BinaryAcademyTask3.Domain;
using BinaryAcademyTask3.Infrastructure;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BinaryAcademyTask3.Application.Teams.Queries.TeamById
{
    public class TeamByIdQueryHandler : IRequestHandler<TeamByIdQuery, TeamDto>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public TeamByIdQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }
        public Task<TeamDto> Handle(TeamByIdQuery request, CancellationToken cancellationToken)
        {
            if (request is null)
                throw new ArgumentNullException(nameof(request));

            var team = _unitOfWork.GetRepository<Team>().GetById(request.Id);

            if (team is null)
                throw new NotImplementedException(nameof(request.Id));

            return System.Threading.Tasks.Task.FromResult(_mapper.Map<TeamDto>(team));
        }
    }
}
