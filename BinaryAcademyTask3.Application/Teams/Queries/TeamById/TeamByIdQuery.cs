﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace BinaryAcademyTask3.Application.Teams.Queries.TeamById
{
    [DataContract]
    public class TeamByIdQuery : IRequest<TeamDto>
    {
        [DataMember]
        public int Id { get; set; }
    }
}
