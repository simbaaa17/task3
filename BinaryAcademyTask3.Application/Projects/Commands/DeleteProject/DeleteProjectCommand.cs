﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace BinaryAcademyTask3.Application.Projects.Commands.DeleteProject
{
    [DataContract]
    public class DeleteProjectCommand : IRequest
    {
        [DataMember]
        public int Id { get; set; }
    }
}
