﻿using BinaryAcademyTask3.Domain;
using BinaryAcademyTask3.Infrastructure;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BinaryAcademyTask3.Application.Projects.Commands.DeleteProject
{
    public class DeleteProjectCommandHandler : IRequestHandler<DeleteProjectCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IGenericRepository<Project> _repository;

        public DeleteProjectCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _repository = unitOfWork.GetRepository<Project>();
        }

        public Task<Unit> Handle(DeleteProjectCommand request, CancellationToken cancellationToken)
        {
            if (request is null)
                throw new ArgumentNullException(nameof(request));

            var projectForDelete = _repository.GetById(request.Id);

            if (projectForDelete is null)
                throw new ArgumentNullException(nameof(projectForDelete));

            _repository.Remove(projectForDelete);
            _unitOfWork.Commit();

            return System.Threading.Tasks.Task.FromResult(Unit.Value);
        }
    }
}
