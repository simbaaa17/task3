﻿using AutoMapper;
using BinaryAcademyTask3.Application.Projects.Queries;
using BinaryAcademyTask3.Domain;
using BinaryAcademyTask3.Infrastructure;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BinaryAcademyTask3.Application.Projects.Commands.UpdateProject
{
    public class UpdateProjectCommandHandler : IRequestHandler<UpdateProjectCommand, ProjectDto>
    {

        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UpdateProjectCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }
        public Task<ProjectDto> Handle(UpdateProjectCommand request, CancellationToken cancellationToken)
        {
            if (request is null)
                throw new ArgumentNullException(nameof(request));

            var project = _unitOfWork.GetRepository<Project>().GetById(request.Id);

            if (project is null)
                throw new ArgumentNullException(nameof(project));

            project.Name = request.Name;

            _unitOfWork.GetRepository<Project>().Update(project);
            _unitOfWork.Commit();

            return System.Threading.Tasks.Task.FromResult(_mapper.Map<ProjectDto>(project));
        }
    }
}
