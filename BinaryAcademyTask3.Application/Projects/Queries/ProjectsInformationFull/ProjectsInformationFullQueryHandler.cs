﻿using AutoMapper;
using BinaryAcademyTask3.Common;
using BinaryAcademyTask3.Domain;
using BinaryAcademyTask3.Infrastructure;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BinaryAcademyTask3.Application.Projects.Queries.ProjectsInformationFull
{
    public class ProjectsInformationFullQueryHandler : IRequestHandler<ProjectsInformationFullQuery, IEnumerable<ProjectInformationDto>>
    {
        private readonly ProjectRepository _repository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ProjectsInformationFullQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _repository = (ProjectRepository)unitOfWork.GetRepository<Project>(true);
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public Task<IEnumerable<ProjectInformationDto>> Handle(ProjectsInformationFullQuery request, CancellationToken cancellationToken)
        {
            if (request is null)
                throw new ArgumentNullException(nameof(request));

            var projectsInformationRecord = _repository.GetProjectsInformation();

            return System.Threading.Tasks.Task.FromResult(_mapper.Map<IEnumerable<ProjectInformationDto>>(projectsInformationRecord));
        }
    }
}
