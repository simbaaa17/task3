﻿using BinaryAcademyTask3.Common;
using MediatR;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace BinaryAcademyTask3.Application.Projects.Queries.ProjectsInformationFull
{
    [DataContract]
    public class ProjectsInformationFullQuery : IRequest<IEnumerable<ProjectInformationDto>>
    {
    }
}
