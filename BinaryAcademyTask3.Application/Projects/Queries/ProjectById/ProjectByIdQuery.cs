﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace BinaryAcademyTask3.Application.Projects.Queries.ProjectById
{
    [DataContract]
    public class ProjectByIdQuery : IRequest<ProjectDto>
    {
        public int Id { get; set; }
    }
}
