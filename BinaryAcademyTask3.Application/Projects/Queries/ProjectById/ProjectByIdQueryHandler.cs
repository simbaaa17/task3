﻿using AutoMapper;
using BinaryAcademyTask3.Domain;
using BinaryAcademyTask3.Infrastructure;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BinaryAcademyTask3.Application.Projects.Queries.ProjectById
{
    public class ProjectByIdQueryHandler : IRequestHandler<ProjectByIdQuery, ProjectDto>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ProjectByIdQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }
        public Task<ProjectDto> Handle(ProjectByIdQuery request, CancellationToken cancellationToken)
        {
            if (request is null)
                throw new ArgumentNullException(nameof(request));

            var project = _unitOfWork.GetRepository<Project>().GetById(request.Id);

            if (project is null)
                throw new NotImplementedException(nameof(request.Id));

            return System.Threading.Tasks.Task.FromResult(_mapper.Map<ProjectDto>(project));
        }
    }
}
