﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace BinaryAcademyTask3.Application.Projects.Queries
{
    [DataContract]
    public class ProjectsQuery : IRequest<IEnumerable<ProjectDto>>
    {

    }
}
