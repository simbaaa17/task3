﻿using AutoMapper;
using BinaryAcademyTask3.Application.Projects.Commands.AddProject;
using BinaryAcademyTask3.Common;
using BinaryAcademyTask3.Domain;
using BinaryAcademyTask3.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace BinaryAcademyTask3.Application.Projects.Queries
{
    public class ProjectDtoMapper : Profile
    {
        public ProjectDtoMapper()
        {
            CreateMap<Project, ProjectDto>();
            CreateMap<AddProjectCommand, Project>();
            CreateMap<ProjectInformationRecord, ProjectInformationDto>();
        }  
    }
}
