﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace BinaryAcademyTask3.Application.Projects.Queries
{
    [DataContract]
    public class ProjectDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }
        
        [DataMember]
        public string Description { get; set; }
        
        [DataMember]
        public DateTime CreatedAt { get; set; }
        
        [DataMember]
        public DateTime Deadline { get; set; }
        
        [DataMember]
        public int AuthorId { get; set; }
        
        [DataMember]
        public int TeamId { get; set; }

    }
}
