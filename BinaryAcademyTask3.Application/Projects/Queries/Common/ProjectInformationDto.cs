﻿using BinaryAcademyTask3.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace BinaryAcademyTask3.Common
{
    public class ProjectInformationDto
    {
        public Project Project { get; set; }
        public Task LongestTask { get; set; }
        public Task ShortestTask { get; set; }
        public int TeamCount { get; set; }
    }
}
