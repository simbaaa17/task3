﻿using BinaryAcademyTask3.Common;
using MediatR;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace BinaryAcademyTask3.Application.Users.Queries.UsersWithTasks
{
    [DataContract]
    public class UsersWithTasksQuery : IRequest<IEnumerable<UserWithTasksDto>>
    {
    }
}
