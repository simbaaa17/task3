﻿using AutoMapper;
using BinaryAcademyTask3.Common;
using BinaryAcademyTask3.Domain;
using BinaryAcademyTask3.Infrastructure;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BinaryAcademyTask3.Application.Users.Queries.UsersWithTasks
{
    public class UsersWithTasksQueryHandler : IRequestHandler<UsersWithTasksQuery, IEnumerable<UserWithTasksDto>>
    {
        private readonly UserRepository _repository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UsersWithTasksQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _repository = (UserRepository)unitOfWork.GetRepository<User>(true);
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public Task<IEnumerable<UserWithTasksDto>> Handle(UsersWithTasksQuery request, CancellationToken cancellationToken)
        {
            if (request is null)
                throw new ArgumentNullException(nameof(request));

            var usersWithTasksRecord = _repository.GetUsersOrderByFirstnameWithTasks();

            return System.Threading.Tasks.Task.FromResult(_mapper.Map<IEnumerable<UserWithTasksDto>>(usersWithTasksRecord));
        }
    }
}
