﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace BinaryAcademyTask3.Application.Users.Queries
{
    [DataContract]
    public class UsersQuery : IRequest<IEnumerable<UserDto>>
    {

    }
}
