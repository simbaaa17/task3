﻿using AutoMapper;
using BinaryAcademyTask3.Application.Users.Commands.AddUser;
using BinaryAcademyTask3.Application.Users.Queries.Common;
using BinaryAcademyTask3.Common;
using BinaryAcademyTask3.Domain;
using BinaryAcademyTask3.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace BinaryAcademyTask3.Application.Users.Queries
{
    public class UserDtoMapper : Profile
    {
        public UserDtoMapper()
        {
            CreateMap<User, UserDto>();
            CreateMap<AddUserCommand, User>();
            CreateMap<UserInformationFullRecord, UserInformationFullDto>();
            CreateMap<UserWithTasksRecord, UserWithTasksDto>();
        }
    }
}
