﻿using BinaryAcademyTask3.Domain;
using System.Collections.Generic;
using System.Linq;

namespace BinaryAcademyTask3.Common
{
    public class UserWithTasksDto
    {
        public User User { get; set; }
        public List<Task> Tasks { get; set; }
    }
}

