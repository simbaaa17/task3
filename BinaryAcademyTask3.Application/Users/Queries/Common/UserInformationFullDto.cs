﻿using BinaryAcademyTask3.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace BinaryAcademyTask3.Application.Users.Queries.Common
{
    public class UserInformationFullDto
    {
        public User User { get; set; }

        public Project LastProject { get; set; }

        public int TasksCount { get; set; }

        public int TasksCanceledCount { get; set; }

        public Task LongestTask { get; set; }
    }
}
