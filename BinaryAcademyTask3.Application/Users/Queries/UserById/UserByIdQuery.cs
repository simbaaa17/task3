﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace BinaryAcademyTask3.Application.Users.Queries.UserById
{
    [DataContract]
    public class UserByIdQuery : IRequest<UserDto>
    {
        [DataMember]
        public int Id { get; set; }
    }
}
