﻿using AutoMapper;
using BinaryAcademyTask3.Domain;
using BinaryAcademyTask3.Infrastructure;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BinaryAcademyTask3.Application.Users.Queries.UserById
{
    public class UserByIdQueryHandler : IRequestHandler<UserByIdQuery, UserDto>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UserByIdQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }
        public Task<UserDto> Handle(UserByIdQuery request, CancellationToken cancellationToken)
        {
            if (request is null)
                throw new ArgumentNullException(nameof(request));

            var user = _unitOfWork.GetRepository<User>().GetById(request.Id);

            if (user is null)
                throw new NotImplementedException(nameof(user));

            return System.Threading.Tasks.Task.FromResult(_mapper.Map<UserDto>(user));
        }
    }
}
