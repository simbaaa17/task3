﻿using BinaryAcademyTask3.Application.Users.Queries.Common;
using BinaryAcademyTask3.Common;
using MediatR;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace BinaryAcademyTask3.Application.Users.Queries.UserInformationFull
{
    [DataContract]
    public class UserInformationFullQuery : IRequest<UserInformationFullDto>
    {
        [DataMember]
        public int Id { get; set; }
    }
}
