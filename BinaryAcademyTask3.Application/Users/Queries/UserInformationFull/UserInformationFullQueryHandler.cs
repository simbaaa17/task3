﻿using AutoMapper;
using BinaryAcademyTask3.Application.Users.Queries.Common;
using BinaryAcademyTask3.Common;
using BinaryAcademyTask3.Domain;
using BinaryAcademyTask3.Infrastructure;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BinaryAcademyTask3.Application.Users.Queries.UserInformationFull
{
    public class UserInformationFullQueryHandler : IRequestHandler<UserInformationFullQuery, UserInformationFullDto>
    {
        private readonly UserRepository _repository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UserInformationFullQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _repository = (UserRepository)unitOfWork.GetRepository<User>(true);
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }


        public Task<UserInformationFullDto> Handle(UserInformationFullQuery request, CancellationToken cancellationToken)
        {
            if (request is null)
                throw new ArgumentNullException(nameof(request));

            var userInformationRecord = _repository.GetUserFullInformation(request.Id);

            return System.Threading.Tasks.Task.FromResult(_mapper.Map<UserInformationFullDto>(userInformationRecord));
        }
    }
}
