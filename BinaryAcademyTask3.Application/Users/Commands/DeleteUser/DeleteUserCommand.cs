﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace BinaryAcademyTask3.Application.Users.Commands.DeleteUser
{
    [DataContract]
    public class DeleteUserCommand : IRequest
    {
        [DataMember]
        public int Id { get; set; }
    }
}
