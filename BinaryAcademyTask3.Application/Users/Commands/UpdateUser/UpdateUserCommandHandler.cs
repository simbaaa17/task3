﻿using AutoMapper;
using BinaryAcademyTask3.Application.Users.Queries;
using BinaryAcademyTask3.Domain;
using BinaryAcademyTask3.Infrastructure;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BinaryAcademyTask3.Application.Users.Commands.UpdateUser
{
    public class UpdateUserCommandHandler : IRequestHandler<UpdateUserCommand, UserDto>
    {

        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UpdateUserCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }
        public Task<UserDto> Handle(UpdateUserCommand request, CancellationToken cancellationToken)
        {
            if (request is null)
                throw new ArgumentNullException(nameof(request));

            var user = _unitOfWork.GetRepository<User>().GetById(request.Id);

            if (user is null)
                throw new ArgumentNullException(nameof(user));

            user.Firstname = request.FirstName;
            user.Lastname = request.LastName;
            user.Birthday = request.Birthday;
            user.Email = request.Email;
            user.TeamId = request.TeamId;

            _unitOfWork.GetRepository<User>().Update(user);
            _unitOfWork.Commit();

            return System.Threading.Tasks.Task.FromResult(_mapper.Map<UserDto>(user));
        }
    }
}
