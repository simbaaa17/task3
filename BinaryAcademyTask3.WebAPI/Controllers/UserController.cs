﻿using BinaryAcademyTask3.Application.Projects.Queries;
using BinaryAcademyTask3.Application.Users.Commands.AddUser;
using BinaryAcademyTask3.Application.Users.Commands.DeleteUser;
using BinaryAcademyTask3.Application.Users.Commands.UpdateUser;
using BinaryAcademyTask3.Application.Users.Queries;
using BinaryAcademyTask3.Application.Users.Queries.Common;
using BinaryAcademyTask3.Application.Users.Queries.UserById;
using BinaryAcademyTask3.Application.Users.Queries.UserInformationFull;
using BinaryAcademyTask3.Application.Users.Queries.UsersWithTasks;
using BinaryAcademyTask3.Common;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace BinaryAcademyTask3.WebAPI.Controllers
{
    [Route("api/users")]
    public class UserController : MediatorController
    {
        public UserController(IMediator mediator) : base(mediator)
        {

        }

        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(IEnumerable<UserDto>))]
        public Task<IActionResult> GetUsers(UsersQuery query) => ExecuteQuery(query);

        [HttpGet("info")]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(IEnumerable<UserWithTasksDto>))]
        public Task<IActionResult> GetUsersWithTasks(UsersWithTasksQuery query) => ExecuteQuery(query);

        [HttpGet("info/{Id}")]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(UserInformationFullDto))]
        public Task<IActionResult> GetUserInformationFullById(UserInformationFullQuery query) => ExecuteQuery(query);


        [HttpGet("{Id}")]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(UserDto))]
        public Task<IActionResult> GetUserById(UserByIdQuery query) => ExecuteQuery(query);

        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        [Produces(typeof(UserDto))]
        public async Task<IActionResult> AddUser([FromBody] AddUserCommand command)
        {
            if (command is null)
                return BadRequest();

            var userId = await _mediator.Send(command);

            return CreatedAtAction("AddUser", new { id = userId });
        }

        [HttpDelete("{Id}")]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> DeleteUser(DeleteUserCommand command)
        {
            if (command is null)
                return BadRequest();

            await _mediator.Send(command);

            return NoContent();
        }

        [HttpPut("update")]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(UserDto))]
        public async Task<IActionResult> UpdateUser([FromBody]  UpdateUserCommand command)
        {
            if (command is null)
                return BadRequest();

            return Json(await _mediator.Send(command));
        }

    }
}
