﻿using BinaryAcademyTask3.Application.Teams.Commands.AddTeam;
using BinaryAcademyTask3.Application.Teams.Commands.DeleteTeam;
using BinaryAcademyTask3.Application.Teams.Commands.UpdateTeam;
using BinaryAcademyTask3.Application.Teams.Queries;
using BinaryAcademyTask3.Application.Teams.Queries.Common;
using BinaryAcademyTask3.Application.Teams.Queries.TeamById;
using BinaryAcademyTask3.Application.Teams.Queries.TeamsInformationFull;
using MediatR;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace BinaryAcademyTask3.WebAPI.Controllers
{
    [Microsoft.AspNetCore.Mvc.Route("api/teams")]
    public class TeamController : MediatorController
    {
        public TeamController(IMediator mediator) : base(mediator)
        {

        }

        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(IEnumerable<TeamDto>))]
        public Task<IActionResult> GetTeams(TeamsQuery query) => ExecuteQuery(query);

        [HttpGet("info")]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(IEnumerable<TeamInformationDto>))]
        public Task<IActionResult> GetFullInfoOfTeams(TeamsInformationFullQuery query) => ExecuteQuery(query);

        [HttpGet("{Id}")]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(TeamDto))]
        public Task<IActionResult> GetTeamById(TeamByIdQuery query) => ExecuteQuery(query);

        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        [Produces(typeof(int))]
        public async Task<IActionResult> AddTeam([FromBody] AddTeamCommand command)
        {

            if (command is null)
                return BadRequest();

            var teamId = await _mediator.Send(command);

            return CreatedAtAction("AddTeam", new { id = teamId });
        }

        [HttpDelete("{Id}")]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> DeleteTeam(DeleteTeamCommand command)
        {
            if (command is null)
                return BadRequest();

            await _mediator.Send(command);

            return NoContent();
        }

        [HttpPut("update")]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(TeamDto))]
        public async Task<IActionResult> UpdateTeam([FromBody]  UpdateTeamCommand command)
        {
            if (command is null)
                return BadRequest();

            return Json(await _mediator.Send(command));
        }

    }
}
