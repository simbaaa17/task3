﻿using BinaryAcademyTask3.Application.Projects.Commands.AddProject;
using BinaryAcademyTask3.Application.Projects.Commands.DeleteProject;
using BinaryAcademyTask3.Application.Projects.Commands.UpdateProject;
using BinaryAcademyTask3.Application.Projects.Queries;
using BinaryAcademyTask3.Application.Projects.Queries.ProjectById;
using BinaryAcademyTask3.Application.Projects.Queries.ProjectsInformationFull;
using BinaryAcademyTask3.Common;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace BinaryAcademyTask3.WebAPI.Controllers
{
    [Route("api/Projects")]
    public class ProjectController : MediatorController
    {
        public ProjectController(IMediator mediator) : base(mediator)
        {

        }

        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(IEnumerable<ProjectDto>))]
        public Task<IActionResult> GetProjects(ProjectsQuery query) => ExecuteQuery(query);

        [HttpGet("info")]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(IEnumerable<ProjectInformationDto>))]
        public Task<IActionResult> GetInformationByProjects(ProjectsInformationFullQuery query) => ExecuteQuery(query);

        [HttpGet("{Id}")]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(ProjectDto))]
        public Task<IActionResult> GetProjectById(ProjectByIdQuery query) => ExecuteQuery(query);

        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        [Produces(typeof(ProjectDto))]
        public async Task<IActionResult> AddProject([FromBody] AddProjectCommand command)
        {
            if (command is null)
                return BadRequest();

            var projectId = await _mediator.Send(command);

            return CreatedAtAction("AddProject", new { id = projectId });
        }

        [HttpDelete("{Id}")]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> DeleteProject(DeleteProjectCommand command)
        {
            if (command is null)
                return BadRequest();

            await _mediator.Send(command);

            return NoContent();
        }

        [HttpPut("update")]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [Produces(typeof(ProjectDto))]
        public async Task<IActionResult> UpdateProject([FromBody]  UpdateProjectCommand command)
        {
            if (command is null)
                return BadRequest();

            return Json(await _mediator.Send(command));
        }

    }
}
