﻿using BinaryAcademyTask3.Domain;
using BinaryAcademyTask3.Infrastructure;
using Microsoft.Extensions.DependencyInjection;

namespace BinaryAcademyTask3.WebAPI.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddDataAccess(this IServiceCollection services)
        {
            services.AddSingleton<ApplicationContext>();

            services.AddTransient<IGenericRepository<Task>, TaskRepository>();
            services.AddTransient<IGenericRepository<User>, UserRepository>();
            services.AddTransient<IGenericRepository<Team>, TeamRepository>();
            services.AddTransient<IGenericRepository<Project>, ProjectRepository>();

            services.AddScoped<IUnitOfWork, UnitOfWork>();
        }
    }
}
