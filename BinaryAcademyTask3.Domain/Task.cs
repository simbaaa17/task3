﻿using BinaryAcademyTask3.Domain.Abstracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace BinaryAcademyTask3.Domain
{
    public class Task : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
        public TaskState TaskState { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }

        public Task(int id) : base(id)
        {

        }
        public override string ToString() => $"{Id} - {Name}";
    }
}
