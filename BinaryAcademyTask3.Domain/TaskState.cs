﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BinaryAcademyTask3.Domain
{
    public enum TaskState
    {
        Created = 0,
        Started,
        Finished,
        Canceled
    }
}
