﻿using BinaryAcademyTask3.Domain.Abstracts;
using System;

namespace BinaryAcademyTask3.Domain
{
    public class User : BaseEntity
    {
        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string Email { get; set; }

        public DateTime Birthday { get; set; }

        public DateTime RegisteredAt { get; set; }

        public int? TeamId { get; set; }

        public User(int id) : base(id)
        {

        }

        public override string ToString()
        {
            return $"{Id} - {Firstname} - {Lastname}";
        }
    }
}
