﻿using BinaryAcademyTask3.Domain.Abstracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace BinaryAcademyTask3.Domain
{
    public class TaskStateModel : BaseEntity
    {
        public string Value { get; set; }

        public TaskStateModel(int id) : base(id)
        {

        }

        public override string ToString() => $"{Id} - {Value}";
    }
}
