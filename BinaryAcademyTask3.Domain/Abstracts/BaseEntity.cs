﻿using BinaryAcademyTask3.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace BinaryAcademyTask3.Domain.Abstracts
{
    public abstract class BaseEntity
    {
        public int Id { get; set; }

        public EntityState EntityState { get; set; }

        public BaseEntity(int id)
        {
            Id = id;
            EntityState = EntityState.Unchanged;
        }

        public BaseEntity Clone()
        {
            return (BaseEntity)this.MemberwiseClone();
        }
    }
}
