﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BinaryAcademyTask3.Infrastructure
{
    public enum EntityState
    {
        Added, 
        Modified,
        Deleted,
        Unchanged
    }
}
