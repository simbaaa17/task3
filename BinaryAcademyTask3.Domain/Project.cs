﻿using BinaryAcademyTask3.Domain.Abstracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace BinaryAcademyTask3.Domain
{
    public class Project : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }
        public int AuthorId { get; set; }
        public int TeamId { get; set; }

        public Project(int id) : base(id)
        {

        }

   
        public override string ToString() => $"{Id} - {Name}";

    }
}
