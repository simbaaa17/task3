﻿using BinaryAcademyTask3.Domain.Abstracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace BinaryAcademyTask3.Domain
{
    public class Team : BaseEntity
    {
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }

        public Team(int id) : base(id)
        {

        }
        public override string ToString() => $"{Id} - {Name}";
    }
}
