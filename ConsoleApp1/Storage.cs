﻿using BinaryAcademyTask3.Domain;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace ConsoleApp1
{
    public class Storage
    {
        private readonly HttpClient _client;
        public List<Project> Projects { get; set; }
        public List<Task> Tasks { get; set; }
        public List<TaskStateModel> TaskStates { get; set; }
        public List<Team> Teams { get; set; }
        public List<User> Users { get; set; }

        public Storage(HttpClient client)
        {
            Projects = new List<Project>();
            Tasks = new List<Task>();
            TaskStates = new List<TaskStateModel>();
            Teams = new List<Team>();
            Users = new List<User>();
            _client = client ?? throw new ArgumentNullException(nameof(client));
        }

        public async System.Threading.Tasks.Task Initialize()
        {
            HttpResponseMessage response = await _client.GetAsync($"api/users");
            var jsonUsers = await response.Content.ReadAsStringAsync();
            Users = JsonConvert.DeserializeObject<List<User>>(jsonUsers);

            response = await _client.GetAsync($"api/teams");
            var jsonTeams = await response.Content.ReadAsStringAsync();
            Teams = JsonConvert.DeserializeObject<List<Team>>(jsonTeams);

            response = await _client.GetAsync($"api/taskStates");
            var jsonTaskStates = await response.Content.ReadAsStringAsync();
            TaskStates = JsonConvert.DeserializeObject<List<TaskStateModel>>(jsonTaskStates);

            response = await _client.GetAsync($"api/tasks");
            var jsonTasks = await response.Content.ReadAsStringAsync();
            Tasks = JsonConvert.DeserializeObject<List<Task>>(jsonTasks);

            response = await _client.GetAsync($"api/projects");
            var jsonProjects = await response.Content.ReadAsStringAsync();
            Projects = JsonConvert.DeserializeObject<List<Project>>(jsonProjects);
        }
    }
}
