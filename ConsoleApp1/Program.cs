﻿using BinaryAcademyTask3.Domain;
using BinaryAcademyTask3.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Transactions;

namespace ConsoleApp1
{
    class Program
    {
        static async System.Threading.Tasks.Task Main(string[] args)
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:54275/");

            var storage = new Storage(client);
            await storage.Initialize();

            /// <summary>
            /// Data structure:
            /// 1.Project
            ///     1.1. Author
            ///     1.2. Team
            ///     1.3. Tasks
            ///         1.3.1. Performer
            /// </summary>
            #region Data Structure
            var projects = (from project in storage.Projects
                            join author in storage.Users on project.AuthorId equals author.Id
                            join team in storage.Teams on project.TeamId equals team.Id
                            select new
                            {
                                project.Id,
                                project.Name,
                                project.Description,
                                project.CreatedAt,
                                project.Deadline,
                                Author = author,
                                Team = team,
                                Tasks = from task in storage.Tasks
                                        join performer in storage.Users on task.PerformerId equals performer.Id
                                        where project.Id == task.ProjectId
                                        select new
                                        {
                                            task.Id,
                                            task.Name,
                                            task.Description,
                                            task.CreatedAt,
                                            task.FinishedAt,
                                            task.TaskState,
                                            Performer = performer
                                        }
                            });

            #endregion

            #region 1
            //Получить кол-во тасков у проекта конкретного пользователя (по id)
            //(словарь, где ключом будет проект, а значением кол-во тасков).

            Dictionary<Project, int> GetTasksCountForUser(int userId)
                => projects
               .ToDictionary(k => GetProjectById(k.Id, storage), i => i.Tasks.Count(t => t.Performer.Id == userId));

            var dictionary = GetTasksCountForUser(3);

            foreach (var keyValue in dictionary)
                Console.WriteLine($"{keyValue.Key} - {keyValue.Value}");

            #endregion

            #region 2
            //Получить список тасков, назначенных на конкретного пользователя(по id), 
            //где name таска < 45 символов(коллекция из тасков).
            IEnumerable<Task> GetTasksForUser(int userId)
                => projects
                    .SelectMany(r => r.Tasks)
                    .Where(t => t.Performer.Id == userId && t.Name.Length < 45)
                    .Select(t => GetTask(t.Id, storage));

            var tasks = GetTasksForUser(3);

            foreach (var task in tasks)
                Console.WriteLine(task);
            #endregion

            #region 3
            //Получить список(id, name) из коллекции тасков, которые выполнены(finished) в текущем(2020)
            //году для конкретного пользователя(по id).
            IEnumerable<(int Id, string Name)> GetTasksFinishedInCurrentYear(int userId)
                => projects
                    .SelectMany(r => r.Tasks)
                    .Where(t => t.Performer.Id == userId && t.FinishedAt.Year == 2020)
                    .Select(t => (t.Id, t.Name));

            var tuple = GetTasksFinishedInCurrentYear(3);

            foreach (var item in tuple)
                Console.WriteLine($"{item.Id} - {item.Name}");
            #endregion

            #region 4
            //Получить список(id, имя команды и список пользователей) из коллекции команд, участники которых старше 10 лет, 
            //отсортированных по дате регистрации пользователя по убыванию, а также сгруппированных по командам.
            IEnumerable<(int Id, string Name, IEnumerable<User> Members)> GetMembersForTeams()
                => from team in storage.Teams
                   join member in storage.Users on team.Id equals member.TeamId
                   where (DateTime.Now.Year - member.Birthday.Year) > 10
                   orderby member.RegisteredAt descending
                   group member by new { team.Id, team.Name } into g
                   select (
                        Id: g.Key.Id,
                        Name: g.Key.Name,
                        Members: from member in g select member);

            var teams = GetMembersForTeams();

            foreach (var team in teams)
            {
                Console.WriteLine($"{team.Id} - {team.Name}");
                Console.WriteLine("Members: ");
                foreach (var member in team.Members)
                {
                    Console.WriteLine(member);
                    Console.WriteLine(DateTime.Now.Year - member.Birthday.Year);
                }
            }

            #endregion

            #region 5
            //Получить список пользователей по алфавиту first_name(по возрастанию) с отсортированными
            //tasks по длине name(по убыванию).
            IEnumerable<(User User, IOrderedEnumerable<Task> Tasks)> GetUsersOrderByFirstname()
                => from performer in storage.Users
                   join task in storage.Tasks on performer.Id equals task.PerformerId
                   orderby performer.Firstname
                   group task by performer into g
                   select (
                    User: g.Key,
                    Tasks: from task in g
                           orderby task.Name.Length descending
                           select task);

            var usersWithTasks = GetUsersOrderByFirstname();

            foreach (var item in usersWithTasks)
            {
                Console.WriteLine(item.User);
                foreach (var task in item.Tasks)
                {
                    Console.WriteLine(task);
                }
            }
            #endregion

            #region 6
            //Получить следующую структуру(передать Id пользователя в параметры):
            // 1. User
            // 2. Последний проект пользователя(по дате создания)
            // 3. Общее кол-во тасков под последним проектом
            // 4. Общее кол-во незавершенных или отмененных тасков для пользователя
            // 5. Самый долгий таск пользователя по дате(раньше всего создан -позже всего закончен
            (User User, Project LastProject, int TasksCount, int TasksCanceledCount, Task LongestTask) GetStructureForFivePoints(int userId)
            {
                return (from user in storage.Users
                        where user.Id == userId
                        select (
                            User: user,
                            LastProject: (from project in storage.Projects
                                         where project.AuthorId == userId
                                         orderby project.CreatedAt descending
                                         select project).FirstOrDefault(),
                            TasksCount: (from task in from task in storage.Tasks
                                         where task.PerformerId == userId
                                         select task
                                         where task.ProjectId == storage.Projects.FirstOrDefault().Id
                                         select task).Count(),
                            TasksCanceledCount: (from task in from task in storage.Tasks
                                                 where task.PerformerId == userId
                                                 select task
                                                 where new List<TaskState>() { TaskState.Canceled, TaskState.Started, TaskState.Created }.Contains(task.TaskState)
                                                 select task).Count(),
                            LongestTask: (from task in from task in storage.Tasks
                                          where task.PerformerId == userId
                                          select task
                                          orderby (task.FinishedAt - task.CreatedAt) descending
                                          select task).FirstOrDefault()
                        )).FirstOrDefault();
            }

            var result = GetStructureForFivePoints(3);

            Console.WriteLine(result.User);
            Console.WriteLine(result.LastProject);
            Console.WriteLine(result.TasksCount);
            Console.WriteLine(result.TasksCanceledCount);
            Console.WriteLine(result.LongestTask);
            #endregion

            #region 7
            //Получить следующую структуру:
            // 1. Проект
            // 2. Самый длинный таск проекта(по описанию)
            // 3. Самый короткий таск проекта(по имени)
            // 4. Общее кол-во пользователей в команде проекта, где или описание проекта > 20 символов или кол - во тасков < 3
            IEnumerable<(Project Project, Task LongestTask, Task ShortestTask, int TeamCount)> GetStructureForFourPoints()
                => from project in projects
                   let tasksInProject = from task in storage.Tasks
                                        where task.ProjectId == project.Id
                                        select task
                   select (

                       Project: GetProjectById(project.Id, storage),
                       LongestTask: (from task in tasksInProject
                                     orderby task.Description.Length descending
                                     select task).FirstOrDefault(),
                       ShortestTask: (from task in tasksInProject
                                      orderby task.Name.Length ascending
                                      select task).FirstOrDefault(),
                       TeamCount: (project.Description.Length > 20 || storage.Tasks.Count < 3)
                                       ? (from task in project.Tasks
                                          select task.Performer).Count()
                                       : 0;
                   );

            var structureFourPoints = GetStructureForFourPoints();

            foreach (var item in structureFourPoints)
            {
                Console.WriteLine(item.Project);
                Console.WriteLine(item.LongestTask);
                Console.WriteLine(item.ShortestTask);
                Console.WriteLine(item.TeamCount);
            }
            #endregion
        }

        private static Project GetProjectById(int id, Storage storage)
          => storage.Projects
              .FirstOrDefault(p => p.Id == id);


        private static Task GetTask(int id, Storage storage)
            => storage.Tasks
                .FirstOrDefault(t => t.Id == id);

    }
}
