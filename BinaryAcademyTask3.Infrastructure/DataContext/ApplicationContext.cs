﻿using BinaryAcademyTask3.Domain;
using BinaryAcademyTask3.Domain.Abstracts;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;

namespace BinaryAcademyTask3.Infrastructure
{    
    public class ApplicationContext : BaseContext
    {
        private const string BASE_ADDRESS = @"https://bsa20.azurewebsites.net/";

        static HttpClient client = new HttpClient();

        public DataSet<User> Users { get; set; }
        public DataSet<Project> Projects { get; set; }
        public DataSet<Domain.Task> Tasks { get; set; }
        public DataSet<TaskStateModel> TaskStateModels { get; set; }
        public DataSet<Team> Teams { get; set; }

        public ApplicationContext()
        {
            client.BaseAddress = new Uri(BASE_ADDRESS);

            Initialize().GetAwaiter().GetResult();
        }
   

        public async System.Threading.Tasks.Task Initialize()
        {
            HttpResponseMessage response = await client.GetAsync($"api/users");
            var jsonUsers = await response.Content.ReadAsStringAsync();
            Users = JsonConvert.DeserializeObject<List<User>>(jsonUsers).ToDataSet(this);

            response = await client.GetAsync($"api/teams");
            var jsonTeams = await response.Content.ReadAsStringAsync();
            Teams = JsonConvert.DeserializeObject<List<Team>>(jsonTeams).ToDataSet(this);

            response = await client.GetAsync($"api/taskStates");
            var jsonTaskStates = await response.Content.ReadAsStringAsync();
            TaskStateModels = JsonConvert.DeserializeObject<List<TaskStateModel>>(jsonTaskStates).ToDataSet(this);

            response = await client.GetAsync($"api/tasks");
            var jsonTasks = await response.Content.ReadAsStringAsync();
            Tasks = JsonConvert.DeserializeObject<List<Domain.Task>>(jsonTasks).ToDataSet(this);

            response = await client.GetAsync($"api/projects");
            var jsonProjects = await response.Content.ReadAsStringAsync();
            Projects = JsonConvert.DeserializeObject<List<Project>>(jsonProjects).ToDataSet(this);
        }
    }
}
