﻿using BinaryAcademyTask3.Domain.Abstracts;
using BinaryAcademyTask3.Infrastructure.Context.Abstract;
using BinaryAcademyTask3.Infrastructure.DataContext.Abstract;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace BinaryAcademyTask3.Infrastructure
{
    public class DataSet<TEntity> where TEntity : BaseEntity
    {
        private List<TEntity> _entities;
        private BaseContext _context;

        public int Count => _entities.Count;

        public DataSet(BaseContext context)
        {
            _entities = new List<TEntity>();
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public DataSet(List<TEntity> entities, BaseContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _entities = entities ?? throw new ArgumentNullException(nameof(entities));
        }

        public IEnumerator<TEntity> GetEnumerator()
        {
            return new EntityEnumerator<TEntity>(_entities);
        }

        public TEntity GetById(int id)
        {
            var entity = _entities.FirstOrDefault(e => e.Id == id);

            _context.ObjectPool.AddItem(id, entity);

            return entity;
        }

        public IEnumerable<TEntity> GetAll()
        {
            foreach (var entity in _entities)
                _context.ObjectPool.AddItem(entity.Id, (TEntity)entity.Clone());

            return _entities;
        }

        public void Add(TEntity entity)
        {
            if (entity is null)
                throw new ArgumentNullException(nameof(entity));

            entity.EntityState = EntityState.Added;

            _context.ObjectPool.AddItem(entity.Id, (TEntity)entity.Clone());
        }

        public void AddToList(TEntity entity)
        {
            _entities.Add(entity);
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            if (entities is null)
                throw new ArgumentNullException(nameof(entities));

            foreach (var entity in entities)
            {
                entity.EntityState = EntityState.Added;
                _context.ObjectPool.AddItem(entity.Id, (TEntity)entity.Clone());
            }
        }


        public void Remove(TEntity entity)
        {
            if (entity is null)
                throw new ArgumentNullException(nameof(entity));

            entity.EntityState = EntityState.Deleted;
            _context.ObjectPool.AddItem(entity.Id, entity);
        }

        public void RemoveFromList(TEntity entity)
        {
            if (entity is null)
                throw new ArgumentNullException(nameof(entity));

            _entities.Remove(entity);
        }


        public void Update(TEntity entity)
        {
            if (entity is null)
                throw new ArgumentNullException(nameof(entity));

            entity.EntityState = EntityState.Modified;
            _context.ObjectPool.AddItem(entity.Id, (TEntity)entity.Clone());
        }

        public List<TEntity> ToList() => _entities;

        public int FindIndex(int id)
        {
           for(int i = 0; i < _entities.Count; i++)
           {
                if (id == _entities[i].Id)
                    return i;
           }

            throw new NotFoundEntityException();
        }

        public void SetItem(int index, TEntity entity)
        {
            _entities[index] = entity;
        }
    }
}
