﻿using BinaryAcademyTask3.Domain.Abstracts;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace BinaryAcademyTask3.Infrastructure.Context.Abstract
{
    class EntityEnumerator<TEntity> : IEnumerator<TEntity> where TEntity : BaseEntity
    {
        private readonly List<TEntity> _entities;
        private int position = -1;
        public EntityEnumerator(List<TEntity> entities)
        {
            _entities = entities;
        }

        public TEntity Current
        {
            get
            {
                if (position == -1 || position >= _entities.Count)
                    throw new InvalidOperationException();
                return _entities[position];
            }
        }

        object IEnumerator.Current => throw new NotImplementedException();

        public bool MoveNext()
        {
            if (position < _entities.Count - 1)
            {
                position++;
                return true;
            }
            else
                return false;
        }

        public void Reset()
        {
            position = -1;
        }
        public void Dispose() { }
    }
}
