﻿using BinaryAcademyTask3.Domain.Abstracts;
using BinaryAcademyTask3.Infrastructure.DataContext.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace BinaryAcademyTask3.Infrastructure
{
    
    public abstract class BaseContext : IDisposable
    {
        private Dictionary<string, object> _storage;

        public ObjectPool ObjectPool;

        public BaseContext()
        {
            ObjectPool = new ObjectPool();
            _storage = new Dictionary<string, object>();
        }

      
        public DataSet<T> Storage<T>()
     where T : BaseEntity
        {
            if (_storage is null)
                _storage = new Dictionary<string, object>();

            var argumentType = typeof(T).Name;

            if (!_storage.ContainsKey(argumentType))
            {
                foreach (var property in GetType().GetProperties())
                {
                    string propertyType = property.PropertyType.GenericTypeArguments[0].Name;

                    if (propertyType == argumentType)
                        _storage.Add(argumentType, property.GetValue(this));
                }
            }

            return (DataSet<T>)_storage[argumentType];
        }

        public int SaveChanges()
        {
            if (!ObjectPool.IsEmpty)
            {
                foreach (var TypeEntity in ObjectPool.Pool)
                {
                    var type = TypeEntity.Key;

                    foreach (var entity in TypeEntity.Value)
                    {
                        var entityState = entity.Value.EntityState;

                        MethodInfo method = null;

                        foreach (var property in GetType().GetProperties())
                        {
                            string propertyType = property.PropertyType.GenericTypeArguments[0].Name;

                            if (propertyType == type.Name)
                            {
                                switch (entityState)
                                {
                                    case EntityState.Added:

                                        method = property.PropertyType.GetMethod("AddToList");
                                        method.Invoke(property.GetValue(this), new object[] { entity.Value });

                                        break;
                                    case EntityState.Deleted:

                                        method = property.PropertyType.GetMethod("RemoveFromList");
                                        method.Invoke(property.GetValue(this), new object[] { entity.Value });

                                        break;
                                    case EntityState.Modified:

                                        method = property.PropertyType.GetMethods().Where(m => m.Name == "FindIndex").FirstOrDefault();

                                        var index = method.Invoke(property.GetValue(this), new object[] { entity.Key });

                                        method = property.PropertyType.GetMethod("SetItem");

                                        method.Invoke(property.GetValue(this), new object[] { index, entity.Value });

                                        break;
                                    case EntityState.Unchanged:
                                        break;
                                }
                                entity.Value.EntityState = EntityState.Unchanged;
                            }
                        }

                        ObjectPool.Dispose();
                    }
                }
            }

            return 1;
        }

        public void Dispose()
        {
            _storage.Clear();
            ObjectPool.Dispose();
        }
    }
}
