﻿using BinaryAcademyTask3.Domain.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BinaryAcademyTask3.Infrastructure.DataContext.Abstract
{
    public class ObjectPool : IDisposable
    {
        public Dictionary<Type, Dictionary<int, BaseEntity>> Pool { get; set; }

        public ObjectPool()
        {
            Pool = new Dictionary<Type, Dictionary<int, BaseEntity>>();
        }
        public bool IsEmpty => !Pool.Any();
        public void AddItem<T>(int pID, T value) where T : BaseEntity
        {
            Type myType = typeof(T);

            if (!Pool.ContainsKey(myType))
            {
                Pool.Add(myType, new Dictionary<int, BaseEntity>());
                Pool[myType].Add(pID, value);
                return;
            }

            if (!Pool[myType].ContainsKey(pID))
            {
                Pool[myType].Add(pID, value);
                return;
            }

            Pool[myType][pID] = value;
        }

        public bool RemoveItem<T>(int pID)
        {
            Type myType = typeof(T);

            if (!Pool.ContainsKey(myType))
                return false;

            if (!Pool[myType].ContainsKey(pID))
                return false;

            return Pool[myType].Remove(pID);
        }

        public T GetItem<T>(int pID) where T : BaseEntity
        {
            Type myType = typeof(T);

            if (!Pool.ContainsKey(myType))
                throw new KeyNotFoundException();

            if (!Pool[myType].ContainsKey(pID))
                throw new KeyNotFoundException();

            return (T)Pool[myType][pID];
        }

        public void Dispose()
        {
            Pool.Clear();
        }
    }
}
