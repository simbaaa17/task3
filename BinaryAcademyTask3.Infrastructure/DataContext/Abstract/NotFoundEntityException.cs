﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BinaryAcademyTask3.Infrastructure.DataContext.Abstract
{
    public class NotFoundEntityException : Exception
    {
        public NotFoundEntityException() { }
        public NotFoundEntityException(string message) : base(message) { }
        public NotFoundEntityException(string message, Exception inner) : base(message, inner) { }
    }
}
