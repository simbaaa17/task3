﻿using BinaryAcademyTask3.Domain.Abstracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BinaryAcademyTask3.Infrastructure
{
    public interface IUnitOfWork : IDisposable
    {
        IGenericRepository<TEntity> GetRepository<TEntity>(bool hasCustomRepository = false) where TEntity : BaseEntity;

        int Commit();
    }

    
}
