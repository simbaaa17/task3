﻿using BinaryAcademyTask3.Domain.Abstracts;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace BinaryAcademyTask3.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {

        private bool _disposed;
        private Dictionary<Type, object> _repositories;
        private readonly IServiceProvider _services;

        public ApplicationContext DbContext { get; }

        public UnitOfWork(ApplicationContext dbContext, IServiceProvider services)
        {
            DbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _services = services ?? throw new ArgumentNullException(nameof(services));
        }

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    // clear repositories
                    _repositories?.Clear();

                    // dispose the db context.
                    DbContext.Dispose();
                }
            }

            _disposed = true;
        }
        public IGenericRepository<TEntity> GetRepository<TEntity>(bool hasCustomRepository = false) where TEntity : BaseEntity
        {
            if (_repositories == null)
            {
                _repositories = new Dictionary<Type, object>();
            }

            if (hasCustomRepository)
            {
                var customRepo = _services.GetService<IGenericRepository<TEntity>>();
                if (customRepo != null)
                {
                    return customRepo;
                }
            }

            var type = typeof(TEntity);
            if (!_repositories.ContainsKey(type))
            {
                _repositories[type] = new EntityRepository<TEntity>(DbContext);
            }

            return (IGenericRepository<TEntity>)_repositories[type];
        }


        public int Commit()
        {
            return DbContext.SaveChanges();
        }

    }
}
