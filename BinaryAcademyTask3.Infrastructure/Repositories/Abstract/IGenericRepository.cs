﻿using BinaryAcademyTask3.Domain.Abstracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace BinaryAcademyTask3.Infrastructure
{
    public interface IGenericRepository<TEntity> where TEntity : BaseEntity
    {
        TEntity GetById(int id);

        IEnumerable<TEntity> GetAll();

        void Add(TEntity entity);

        void AddRange(IEnumerable<TEntity> entities);

        void Remove(TEntity entity);

        void Update(TEntity entity);
    }
}
