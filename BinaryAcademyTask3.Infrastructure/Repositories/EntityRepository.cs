﻿using BinaryAcademyTask3.Domain.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace BinaryAcademyTask3.Infrastructure
{
    public class EntityRepository<TEntity> : IGenericRepository<TEntity>
        where TEntity : BaseEntity
    {
        protected readonly ApplicationContext _dbContext;
        public EntityRepository(ApplicationContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public TEntity GetById(int id) => _dbContext.Storage<TEntity>().GetById(id);

        public IEnumerable<TEntity> GetAll() => _dbContext.Storage<TEntity>().GetAll();
        public void Add(TEntity entity)
        {
            if (entity is null)
                throw new ArgumentNullException(nameof(entity));

            _dbContext.Storage<TEntity>().Add(entity);
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            if (entities is null)
                throw new ArgumentNullException(nameof(entities));

            foreach (var entity in entities)
                Add(entity);
        }

        public void Remove(TEntity entity)
        {
            if (entity is null)
                throw new ArgumentNullException(nameof(entity));

            _dbContext.Storage<TEntity>().Remove(entity);
        }

        //public IEnumerable<TEntity> FindBy(Expression<Func<TEntity, bool>> predicate)
        //{
        //    var entities = _dbContext.Storage<TEntity>().ToList().AsQueryable().Where(predicate);

        //    foreach (var entity in entities)
        //        _dbContext.ObjectPool.AddItem(entity.Id, (TEntity)entity.Clone());

        //    return entities;
        //}

        public void Update(TEntity entity)
        {
            if (entity is null)
                throw new ArgumentNullException(nameof(entity));

            _dbContext.Storage<TEntity>().Update(entity);
        }
    }


}
