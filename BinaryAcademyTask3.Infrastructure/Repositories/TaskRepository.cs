﻿
using BinaryAcademyTask3.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BinaryAcademyTask3.Infrastructure
{
    public class TasksCountInUserRecord
    {
        public Dictionary<Project, int> Tasks { get; set; }
    }
    public class TaskShortInformationRecord
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class ProjectRecord
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }
        public User Author { get; set; }
        public Team Team { get; set; }
        public IEnumerable<TaskRecord> Tasks { get; set; }
        
    }

    public class TaskRecord
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
        public TaskState TaskState { get; set; }
        public User Performer { get; set; }
    }

    public class TaskRepository : EntityRepository<Task>
    {
        private IEnumerable<ProjectRecord> ProjectSet => from project in _dbContext.Projects.ToList()
                                                        join author in _dbContext.Users.ToList() on project.AuthorId equals author.Id
                                                        join team in _dbContext.Teams.ToList() on project.TeamId equals team.Id
                                                        select new ProjectRecord
                                                        {
                                                            Id = project.Id,
                                                            Name = project.Name,
                                                            Description = project.Description,
                                                            CreatedAt = project.CreatedAt,
                                                            Deadline = project.Deadline,
                                                            Author = author,
                                                            Team = team,
                                                            Tasks = from task in _dbContext.Tasks.ToList()
                                                                    join performer in _dbContext.Users.ToList() on task.PerformerId equals performer.Id
                                                                    where project.Id == task.ProjectId
                                                                    select new TaskRecord
                                                                    {
                                                                        Id = task.Id,
                                                                        Name = task.Name,
                                                                        Description = task.Description,
                                                                        CreatedAt = task.CreatedAt,
                                                                        FinishedAt = task.FinishedAt,
                                                                        TaskState = task.TaskState,
                                                                        Performer = performer
                                                                    }
                                                        };


        public TaskRepository(ApplicationContext context) : base(context)
        {

        }

        public TasksCountInUserRecord GetTasksCountForUser(int userId)
        {
            return new TasksCountInUserRecord(){
                Tasks = ProjectSet
               .ToDictionary(k => GetProjectById(k.Id), i => i.Tasks.Where(t => t.Performer.Id == userId)
               .Count())
            };
        }
        public IEnumerable<Task> GetTasksForUser(int userId)
                => ProjectSet
                    .SelectMany(r => r.Tasks)
                    .Where(t => t.Performer.Id == userId && t.Name.Length < 45)
                    .Select(t => GetTask(t.Id));

        public IEnumerable<TaskShortInformationRecord> GetTasksFinishedInCurrentYear(int userId)
                => ProjectSet
                    .SelectMany(r => r.Tasks)
                    .Where(t => t.Performer.Id == userId && t.FinishedAt.Year == 2020)
                    .Select(t => new TaskShortInformationRecord
                    {
                        Id = t.Id,
                        Name = t.Name
                    });

        private Project GetProjectById(int id)
          => _dbContext.Projects.ToList()
              .FirstOrDefault(p => p.Id == id);

        private Task GetTask(int id)
           => _dbContext.Tasks.ToList()
               .FirstOrDefault(t => t.Id == id);

    }
}
