﻿
using BinaryAcademyTask3.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BinaryAcademyTask3.Infrastructure
{
    public class TeamInformationRecord
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public IEnumerable<User> Members { get; set; }
    }
    public class TeamRepository : EntityRepository<Team>
    {
        public TeamRepository(ApplicationContext context) : base(context)
        {

        }

        public IEnumerable<TeamInformationRecord> GetMembersForTeams()
               => from team in _dbContext.Teams.ToList()
                  join member in _dbContext.Users.ToList() on team.Id equals member.TeamId
                  where (DateTime.Now.Year - member.Birthday.Year) > 10
                  orderby member.RegisteredAt descending
                  group member by new { team.Id, team.Name } into g
                  select new TeamInformationRecord
                  {
                      Id = g.Key.Id,
                      Name = g.Key.Name,
                      Members = from member in g select member
                  };

    }
}
