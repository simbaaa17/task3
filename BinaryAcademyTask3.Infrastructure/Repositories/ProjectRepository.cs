﻿
using BinaryAcademyTask3.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BinaryAcademyTask3.Infrastructure
{
    public class ProjectInformationRecord
    {
        public Project Project { get; set; }
        public Task LongestTask { get; set; }
        public Task ShortestTask { get; set; }
        public int TeamCount { get; set; }
    }

    public class ProjectRepository : EntityRepository<Project>
    {
        private IEnumerable<ProjectRecord> ProjectSet => from project in _dbContext.Projects.ToList()
                                                         join author in _dbContext.Users.ToList() on project.AuthorId equals author.Id
                                                         join team in _dbContext.Teams.ToList() on project.TeamId equals team.Id
                                                         select new ProjectRecord
                                                         {
                                                             Id = project.Id,
                                                             Name = project.Name,
                                                             Description = project.Description,
                                                             CreatedAt = project.CreatedAt,
                                                             Deadline = project.Deadline,
                                                             Author = author,
                                                             Team = team,
                                                             Tasks = from task in _dbContext.Tasks.ToList()
                                                                     join performer in _dbContext.Users.ToList() on task.PerformerId equals performer.Id
                                                                     where project.Id == task.ProjectId
                                                                     select new TaskRecord
                                                                     {
                                                                         Id = task.Id,
                                                                         Name = task.Name,
                                                                         Description = task.Description,
                                                                         CreatedAt = task.CreatedAt,
                                                                         FinishedAt = task.FinishedAt,
                                                                         TaskState = task.TaskState,
                                                                         Performer = performer
                                                                     }
                                                         };


        public ProjectRepository(ApplicationContext context) : base(context)
        {

        }
        public IEnumerable<ProjectInformationRecord> GetProjectsInformation()
                   => from project in ProjectSet
                      let tasksInProject = from task in _dbContext.Tasks.ToList()
                                           where task.ProjectId == project.Id
                                           select task
                      select new ProjectInformationRecord
                      {
                          Project = GetProjectById(project.Id),
                          LongestTask = (from task in tasksInProject
                                         orderby task.Description.Length descending
                                         select task).FirstOrDefault(),
                          ShortestTask = (from task in tasksInProject
                                          orderby task.Name.Length ascending
                                          select task).FirstOrDefault(),
                          TeamCount = (project.Description.Length > 20 || _dbContext.Tasks.Count < 3)
                                          ? (from task in project.Tasks
                                             select task.Performer).Count()
                                          : 0
                      };

        private Project GetProjectById(int id)
          => _dbContext.Projects.ToList()
              .FirstOrDefault(p => p.Id == id);
    }
}
