﻿
using BinaryAcademyTask3.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BinaryAcademyTask3.Infrastructure
{
    public class UserWithTasksRecord
    {
        public User User { get; set; }
        public List<Task> Tasks { get; set; }
    }

    public class UserInformationFullRecord
    {
        public User User { get; set; }

        public Project LastProject { get; set; }

        public int TasksCount { get; set; }

        public int TasksCanceledCount { get; set; }

        public Task LongestTask { get; set; }
    }

    public class UserRepository : EntityRepository<User>
    {
        public UserRepository(ApplicationContext context) : base(context)
        {

        }
                       

        public IEnumerable<UserWithTasksRecord> GetUsersOrderByFirstnameWithTasks()
               => from performer in _dbContext.Users.ToList()
                  join task in _dbContext.Tasks.ToList() on performer.Id equals task.PerformerId
                  orderby performer.Firstname
                  group task by performer into g
                  select new UserWithTasksRecord
                  {
                      User = g.Key,
                      Tasks = (from task in g
                              orderby task.Name.Length descending
                              select task).ToList()
                  };

        public UserInformationFullRecord GetUserFullInformation(int userId)
        {
            var inProcess = new List<TaskState>() { TaskState.Canceled, TaskState.Started, TaskState.Created };

            return (from user in _dbContext.Users.ToList()
                    where user.Id == userId
                    let allProjectsForUser = from project in _dbContext.Projects.ToList()
                                             where project.AuthorId == userId
                                             orderby project.CreatedAt descending
                                             select project
                    let allTasksForUser = from task in _dbContext.Tasks.ToList()
                                          where task.PerformerId == userId
                                          select task
                    select new UserInformationFullRecord
                    {
                         User = user,
                        LastProject = allProjectsForUser.FirstOrDefault(),
                        TasksCount = (from task in allTasksForUser
                                     where task.ProjectId == _dbContext.Projects.ToList().FirstOrDefault().Id
                                     select task).Count(),
                        TasksCanceledCount = (from task in allTasksForUser
                                             where inProcess.Contains(task.TaskState)
                                             select task).Count(),
                        LongestTask = (from task in allTasksForUser
                                      orderby (task.FinishedAt - task.CreatedAt) descending
                                      select task).FirstOrDefault()
                    }).FirstOrDefault();
        }
    }
}
